<?php
/* 
 * Fuction to return a list of devices with attached sensors
 * If deviceID is provide the function will only return sensors connected 
 * to this given device
 */
function get_devices_sensors_db($deviceID = NULL) {

  db_set_active();

  $query = db_select('currentcost_client_sensor', 's');
  $query->join('currentcost_client_device', 'd', 
      'd.deviceID = s.deviceID');
  $query->join('currentcost_client_sensor_type', 't', 
      's.typeID = t.typeID');
  $query->fields('d', array('deviceID','name','description'));
  $query->fields('s', array('sensorID','name','typeID', 'master', 'description'));
  $query->fields('t', array('type', 'description','units'));
  if($deviceID != NULL) {
    $query->condition('d.deviceID', $deviceID);
  }
  $result = $query->execute()->fetchAllAssoc('sensorID');
  return $result;

}

/* 
 * Fuction to return a list of devices with attached profiles
 * If deviceID is provide the function will only return profiles connected 
 * to this given device
 */
function get_devices_profiles_db($deviceID = NULL, $sensorID = NULL) {

  db_set_active();

  $query = db_select('currentcost_client_sensor_profile', 'p');
  $query->join('currentcost_client_sensor', 's', 
      's.sensorID = p.sensorID');
  $query->join('currentcost_client_device', 'd', 
      'd.deviceID = s.deviceID');
  $query->join('currentcost_client_sensor_type', 't', 
      's.typeID = t.typeID');
  $query->fields('s', array('sensorID','name','typeID', 'master'));
  $query->fields('t', array('type', 'description','units'));
  $query->fields('p', array('profileID', 'name','description','date','end_date'));
  if($deviceID != NULL) {
    $query->condition('d.deviceID', $deviceID);
  }
  if($sensorID != NULL) {
    $query->condition('s.sensorID', $sensorID);
  }
  $result = $query->execute()->fetchAllAssoc('profileID');

  return $result;

}

/*
 * Function to return readings related to a profile.
 *  if $end_date is null then it is assumed that an
 *  end_date field is not present and it will return all
 *  dates up to now. - needs a better implimentation...
 */
function get_profile_readings_db($profileID) {
  db_set_active();
  $query_date_test = db_select('currentcost_client_sensor_profile', 'p');
  $query_date_test->condition('p.profileID', $profileID);
  $query_date_test->fields('p', array('end_date'));
  $result_date_test = $query_date_test->execute()->fetchAssoc();
  
  $query = db_select('currentcost_client_measurement', 'm');
  $query->join('currentcost_client_reading', 'r', 
      'r.readingID = m.readingID');
  $query->join('currentcost_client_sensor', 's', 
      'r.sensorID = s.sensorID');
  $query->join('currentcost_client_sensor_profile', 'p', 
      's.sensorID = p.sensorID');
  $query->fields('r', array('date'));
  $query->fields('m', array('measurement'));
  $query->orderBy('r.date','ASC');
  $query->condition('p.profileID', $profileID);
  if($result_date_test['end_date'] == NULL) {
    $query->where('r.date BETWEEN p.date AND NOW()');
  } else {
    $query->where('r.date BETWEEN p.date AND p.end_date');
  }
  $result = $query->execute()->fetchAllAssoc('date');
  return($result);

}



/* 
 * Fuction to return a sensor's data
 */
function get_sensor_db($sensorID) {

  db_set_active();

  $query = db_select('currentcost_client_sensor', 's');
  $query->join('currentcost_client_sensor_type', 't', 
      's.typeID = t.typeID');
  $query->fields('t', array('units','type','description'));
  $query->fields('s', array('sensorID','deviceID','name','description', 'typeID', 'master'));
  $query->condition('s.sensorID', $sensorID);
  $result = $query->execute()->fetchAssoc();

  return $result;

}

/* 
 * Fuction to return a profile's data
 */
function get_profile_db($profileID) {

  db_set_active();

  $query = db_select('currentcost_client_sensor_profile', 'p');
  $query->join('currentcost_client_sensor', 's', 
      's.sensorID = p.sensorID');
  $query->join('currentcost_client_sensor_type', 't', 
      's.typeID = t.typeID');
  $query->fields('p', array('name','description','date','end_date'));
  $query->fields('t', array('units','type','description'));
  $query->fields('s', array('sensorID','deviceID','name','description','typeID', 'master'));
  $query->condition('p.profileID', $profileID);
  $result = $query->execute()->fetchAssoc();

  return $result;

}

/* 
 * Fuction to return a list of devices
 */
function get_devices_db() {

  db_set_active();

  $query = db_select('currentcost_client_device', 'd');
  $query->fields('d', array('deviceID','name','description'));
  $result = $query->execute()->fetchAllAssoc('deviceID');

  return $result;

}


/* 
 * Fuction to return a current reading from a sensorID
 */
function get_reading_db($sensorID, $date = NULL) {

  db_set_active();

  $query = db_select('currentcost_client_reading', 'r');
  $query->join('currentcost_client_measurement', 'm', 
      'r.readingID = m.readingID');
  $query->join('currentcost_client_sensor', 's', 
      'r.sensorID = s.sensorID');
  $query->fields('r', array('date'));
  $query->fields('m', array('measurement'));
  $query->orderBy('r.date','DESC')->range(0,1);
  if($date != NULL) {
    $query->where('r.date <= \'' . $date . '\'' );
  }
  $query->condition('s.sensorID', $sensorID);
  $result = $query->execute()->fetchAssoc();

  return $result;

}

/************ The following tw functions should be removed in place of a more dynamic 
 *           call to the get_current_reading_db($sensorID)...
 */
/* 
 * function to return current reading of sensor 
 */
function get_current_power_reading_db($sensorID) {

  db_set_active();

  $query = db_select('currentcost_client_reading', 'r');
  $query->join('currentcost_client_measurement', 'm', 
      'r.readingID = m.readingID');
  $query->join('currentcost_client_sensor', 's', 
      'r.sensorID = s.sensorID');
  $query->fields('r', array('date'));
  $query->fields('m', array('measurement'));
  $query->orderBy('r.date','DESC')->range(0,1);
  $query->condition('s.typeID', '1');
  $query->condition('s.sensorID', $sensorID);
  $result = $query->execute()->fetchAssoc();

  return $result;

}
function get_current_temp_reading_db($sensorID) {

  db_set_active();

  $query = db_select('currentcost_client_reading', 'r');
  $query->join('currentcost_client_measurement', 'm', 
      'r.readingID = m.readingID');
  $query->join('currentcost_client_sensor', 's', 
      'r.sensorID = s.sensorID');
  $query->fields('r', array('date'));
  $query->fields('m', array('measurement'));
  $query->orderBy('r.date','DESC')->range(0,1);
  $query->condition('s.typeID', '9');
  $query->condition('s.sensorID', $sensorID);
  $result = $query->execute()->fetchAssoc();

  return $result;

}

function get_last_nhour_measurements_db($sensorID, $hours) {
  db_set_active();
  $query = db_select('currentcost_client_measurement', 'm');
  $query->join('currentcost_client_reading', 'r', 
      'r.readingID = m.readingID');
  $query->join('currentcost_client_sensor', 's', 
      'r.sensorID = s.sensorID');
  $query->fields('r', array('date'));
  $query->fields('m', array('measurement'));

  $query->orderBy('r.date','ASC');
  $query->where('r.date >= DATE_SUB(NOW(), INTERVAL ' . $hours . ' HOUR)');
  $query->condition('s.sensorID', $sensorID);
  $result = $query->execute()->fetchAllAssoc('date');
  return($result);

}

function get_last_nhour_temps_db($sensorID, $hours) {

  return(get_last_nhour_measurements_db($sensorID, $hours));

}
function get_last_nhour_powers_db($sensorID, $hours) {

  return(get_last_nhour_measurements_db($sensorID, $hours));

}

/* need a generic max min finction */

function get_last_nhour_maxmin_temp_db($hours) {
  db_set_active();
  $query = db_select('currentcost_client_reading', 'r');
  $query->join('currentcost_client_measurement', 'tm', 
      'r.readingID = tm.readingID');
  $query->join('currentcost_client_sensor', 's', 
      'r.sensorID = s.sensorID');
  $query->fields('r', array('date'));
  $query->fields('tm', array('measurement'));
  $query->where('tm.measurement = (SELECT MAX(measurement) FROM currentcost_client_measurement)');
  $query->condition('s.typeID', '9');
  $result_max = $query->execute()->fetchAssoc();

  $query = db_select('currentcost_client_reading', 'r');
  $query->join('currentcost_client_measurement', 'tm', 
      'r.readingID = tm.readingID');
  $query->join('currentcost_client_sensor', 's', 
      'r.sensorID = s.sensorID');
  $query->fields('r', array('date'));
  $query->fields('tm', array('measurement'));
  $query->where('tm.measurement = (SELECT MIN(measurement) FROM currentcost_client_measurement)');
  $query->condition('s.typeID', '9');
  $result_min = $query->execute()->fetchAssoc();

  return array(
      'min' => array(
        'temp' => $result_min['measurement'],
        'date' => $result_min['date']),
      'max' => array(
        'temp' => $result_max['measurement'],
        'date' => $result_max['date']),
      );
}

function get_last_nhour_maxmin_power_db($hours) {
  db_set_active();
  $query = db_select('currentcost_client_reading', 'r');
  $query->join('currentcost_client_measurement', 'pm', 
      'r.readingID = pm.readingID');
  $query->join('currentcost_client_sensor', 's', 
      'r.sensorID = s.sensorID');
  $query->fields('r', array('date'));
  $query->fields('tm', array('measurement'));
  $query->where('pm.measurement = (SELECT MAX(measurement) FROM currentcost_client_measurement_int)');
  $query->condition('s.typeID', '1');
  $result_max = $query->execute()->fetchAssoc();


  $query = db_select('currentcost_client_reading', 'r');
  $query->join('currentcost_client_measurement', 'pm', 
      'r.readingID = pm.readingID');
  $query->join('currentcost_client_sensor', 's', 
      'r.sensorID = s.sensorID');
  $query->fields('r', array('date'));
  $query->fields('tm', array('measurement'));
  $query->where('pm.measurement = (SELECT MIN(measurement) FROM currentcost_client_measurementi_int)');
  $query->condition('s.typeID', '1');
  $result_min = $query->execute()->fetchAssoc();


  return array(
      'max' => array($result_min->measurement,$result_min->date),
      'min' => array($result_max->measurement,$result_max->date),
      );
}


function get_date_avg_db($sensorID, $date) {
  /* $date should be in mysql 'date' format. YYYY-MM-DD */
  db_set_active();
  $query = db_select('currentcost_client_measurement', 'tm');
  $query->join('currentcost_client_reading', 'r', 
      'r.readingID = tm.readingID');
  $query->join('currentcost_client_sensor', 's', 
      'r.sensorID = s.sensorID');
  $query->addExpression('AVG(measurement)');
  $query->where('r.date BETWEEN \'' .  $date . ' 00:00:00\''
      . ' AND DATE_ADD(\'' . $date . ' 00:00:00\', INTERVAL 1 DAY)');
  $query->condition('s.sensorID', $sensorID);
  $result = $query->execute()->fetchAssoc();
  return($result);
}
/* 
 * function returns average for a measument for a particular day, $date, at a particuar 
 * date duration, $date_end - $date_start 
 */
function get_interval_avg_db($sensorID, $date_start, $date_end) {
  /* $date should be in mysql 'datetime' format. YYYY-MM-DD */
  db_set_active();
  $query = db_select('currentcost_client_measurement', 'tm');
  $query->join('currentcost_client_reading', 'r', 
      'r.readingID = tm.readingID');
  $query->join('currentcost_client_sensor', 's', 
      'r.sensorID = s.sensorID');
  $query->addExpression('AVG(measurement)');
  $query->where('r.date BETWEEN \'' .  $date_start . '\''
      . ' AND \'' . $date_end . '\'');
  $query->condition('s.sensorID', $sensorID);
  $result = $query->execute()->fetchAssoc();
  return($result);
}


function get_sensor_calc_db($sensorID, $calctype, $date, $interval) {
  db_set_active();
  $query = db_select('currentcost_client_sensor_calc', 'c');
  $query->where('c.date BETWEEN \'' .  $date . ' 00:00:00\''
      . ' AND DATE_ADD(\'' . $date . ' 00:00:00\', INTERVAL 1 DAY)');
  $query->condition('c.sensorID', $sensorID);
  $query->condition('c.calc_type', $calctype);
  $query->condition('c.time_interval', $interval);
  $query->where('c.expires >= NOW() OR c.expires IS NULL');
  $query->fields('c', array('result'));
  $result = $query->execute()->fetchAssoc();
  return($result);
}

/* INSERT QUERIES */

function insert_sensor_calc_db($sensorID, $calctype, $date, $interval, $result, $expires = NULL) {
  if($sensorID != 0) {
    $tmp = db_insert('currentcost_client_sensor_calc')
      ->fields(array(
            'sensorID' => $sensorID,	
            'calc_type' => $calctype,
            'date' => $date,
            'time_interval' => $interval,
            'result' => $result,
            'expires' => $expires,
            ))
      ->execute();
    return($tmp);
  }
  return(-1);
}

function insert_profile_db($sensorID, $name, $description, $date, $end_date = NULL) {
  if($sensorID != 0) {
    $tmp = db_insert('currentcost_client_sensor_profile')
      ->fields(array(
            'sensorID' => $sensorID,	
            'name' => $name,
            'description' => $description,
            'date' => $date,
            'end_date' => $end_date,
            ))
      ->execute();
    return($tmp);
  }
  return(-1);
}
