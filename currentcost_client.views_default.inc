<?php

function currentcost_client_views_default_views() {
  /* View to show current power consumption in a block */

  /* View to show a list of sensors by deviceID */
  $view = new view();
  $view->name = 'current_cost_sensors';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'currentcost_client_device';
  $view->human_name = 'Current Cost Sensors';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Current Cost Sensors';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table_megarows';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Currentcost devices: deviceID */
  $handler->display->display_options['fields']['deviceID']['id'] = 'deviceID';
  $handler->display->display_options['fields']['deviceID']['table'] = 'currentcost_client_device';
  $handler->display->display_options['fields']['deviceID']['field'] = 'deviceID';
  /* Field: Currentcost devices: Device Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'currentcost_client_device';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: Currentcost Sensors: Sensor Name */
  $handler->display->display_options['fields']['name_2']['id'] = 'name_2';
  $handler->display->display_options['fields']['name_2']['table'] = 'currentcost_client_sensor';
  $handler->display->display_options['fields']['name_2']['field'] = 'name';
  /* Field: Currentcost Sensors: Sensor Description */
  $handler->display->display_options['fields']['description_1']['id'] = 'description_1';
  $handler->display->display_options['fields']['description_1']['table'] = 'currentcost_client_sensor';
  $handler->display->display_options['fields']['description_1']['field'] = 'description';
  /* Field: Currentcost sensor types: Sensor Type */
  $handler->display->display_options['fields']['name_1']['id'] = 'name_1';
  $handler->display->display_options['fields']['name_1']['table'] = 'currentcost_client_sensor_type';
  $handler->display->display_options['fields']['name_1']['field'] = 'name';
  /* Field: Currentcost sensor types: Sensor Units */
  $handler->display->display_options['fields']['units']['id'] = 'units';
  $handler->display->display_options['fields']['units']['table'] = 'currentcost_client_sensor_type';
  $handler->display->display_options['fields']['units']['field'] = 'units';
  /* Field: Currentcost Sensors: RadioID */
  $handler->display->display_options['fields']['radioID']['id'] = 'radioID';
  $handler->display->display_options['fields']['radioID']['table'] = 'currentcost_client_sensor';
  $handler->display->display_options['fields']['radioID']['field'] = 'radioID';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'current-cost-sensors';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '5';

  $views[$view->name] = $view;
  return $views;
}

