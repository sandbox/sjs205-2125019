<?php


require_once ( dirname(__FILE__) . '/currentcost_client_helper.inc');
require_once ( dirname(__FILE__) . '/currentcost_client_charts.inc');

/** 
 * implementation of hook_block_view()
 * see 
 * http://drupal.stackexchange.com/questions/46227/creating-multiple-blocks-programmatically
 for details of multiple blocks
 */
function currentcost_client_block_view($delta = '') {
  $block = array();
  switch($delta) {
    case 'current_reading_chart':
      $block = array(
          'subject' => t(''),
          'content' => current_reading_chart_block(),
          );
      break;
    case 'last24h_reading_chart':
      $block = array(
          'subject' => t(''),
          'content' => last24h_reading_chart_block(),
          );
      break;

  }
  return $block;

}
function current_power_stats_block($sensorID) {

  $block=array();
  $power_cur = get_current_power_reading_db($sensorID);


  $block['title'] = array(
      '#markup' => '<div id="current_power_stats">' 
      );

  $block['current_power_watts'] = array(
      '#markup' => '<h4>Current Power: ' 
      . ($power_cur['measurement'] == NULL ? 0 : $power_cur['measurement'])
      . ' Watts</h4>'
      );

  $kwh_cur = calc_watts_kwh($power_cur['measurement']);
  $block['current_power_kwh'] = array(
      '#markup' => '<h5> ' . $kwh_cur . ' (KWh)</h5>'
      );
  $cost_day = calc_kwh_day_cost($kwh_cur);
  $block['current_power_kwh'] = array(
      '#markup' => '<div class="costs_title"><h4> Costs:</h4></div>'
      . '<div class="costs">'
      . '<h4>Hour: £' . round($cost_day/24,4) . '</h4>'
      . '<h4>Day: £' . $cost_day . '</h4>'
      . '<h4>Month: £' . $cost_day*30 . '</h4>'
      . '</div>'
      );
  $block['end'] = array(
      '#markup' => '</div>'
      );

  return($block);
}
/*
 * Function to display sensor stats 
 */
function sensor_stats_block($sensor) {

  $block=array();

  $block['sensor_description'] = array(
      '#markup' => '<h4><b>Sensor:</b> ' . $sensor['name'] . ' Stats</h4>'
      . '<h4><b>Description:</b> ' . $sensor['description'] . '</h4>'
      );

  $block['sensor_type'] = array(
      '#markup' => '<h4><b>Sensor Type:</b> ' . $sensor['type_desc'] . '</h4>'
      . '<h4><b>Sensor Measurement (Units):</b> ' . $sensor['type'] 
      . ' (' . $sensor['units'] . ')</h4>'
      );

  $block['sensor_device'] = array(
      '#markup' => '<h4><b>Sensor Device:</b> <a href="?q=cc_client&deviceID=' 
      . $sensor['deviceID'] . '">' . $sensor['deviceID'] . '</a></h4>'
      );

  return($block);
}
/*
 * Function to display profile stats 
 */
function profile_stats_block($profile) {

  /* format interval */
  if($profile['interval'] <= 60) {
    $interval = $profile['interval'];
    $units = 'minutes';
  } elseif($profile['interval'] > 60 && $profile['interval'] <= 2880) {
    $interval = round($profile['interval']/60,2);
    $units = 'hours';
  } else {
    $interval = round($profile['interval']/1440,2);
    $units = 'days';
  }
  $block=array();


  $block['profile_description'] = array(
      '#markup' => '<h4><b>Profile:</b> ' . $profile['name'] . '</h4>'
      . '<h4><b>Description:</b> ' . $profile['description'] . '</h4>'
      );

  $block['profile_dates'] = array(
      '#markup' => '<h4><b>Start Date:</b> ' . $profile['date'] . ' </h4>'
      . '<h4><b>End Date:</b> ' 
      . (isset($profile['end_date']) ? $profile['end_date'] : 'Not Set') 
      . ' </h4>'
      . '<h4><b>Interval:</b> ' . $interval . ' ' . $units . '</h4>'
      );
  $block['profile_max_min'] = array(
      '#markup' => '<h4><b>Max:</b>' . $profile['maxmin']['max']['reading'] 
      . ' ' . $profile['sensor']['units']  . ' <b>Min:</b> ' 
      . $profile['maxmin']['min']['reading'] . ' ' . $profile['sensor']['units'] . '</h4>'
      );

  /*$block['profile_fieldset']['reading_count'] = array(
      '#markup' => '<h4><b>No. of Readings:</b> ' . 'XXXXXXXX ' . '</h4>'
      );
*/
  return($block);
}
function current_reading_chart_block() {
  $sensorID = 3;
  $power_cur = get_current_power_reading_db($sensorID);
  $temp_cur = get_current_temp_reading_db($sensorID);
  $block = array(); 
  $block['power_chart'] = render_current_power($power_cur['measurement']);
  $block['temp_chart'] = render_current_temp($temp_cur['measurement']);

  /* Average Reading Today */
  //check no average exists. if not, calculate average:
  //  calc_average();

  $dates = get_last7_dates();

  $results = get_date_calc($sensorID, $dates, 'DAY', 'AVG');
  $block['avg_bar'] =  render_avg_bar_chart($results, 'daily_comparison');

  $grouped_bar_data = get_dates_interval_avgs($sensorID, $dates);
  $block['period_avg'] = render_avg_groupbar_chart($grouped_bar_data, 'Daily_period_comparison');

  return $block;
}
/*
 * Block to show the previous days period avgs
 */
function yesterday_avg_block($sensorID) {

  $date = get_day_interval_avgs($sensorID, date('Y-m-d',strtotime('now') - 86400));

  $bar_data['night'][] = round($date['night'],0);
  $bar_data['morning'][] = round($date['morning'],0);
  $bar_data['afternoon'][] = round($date['afternoon'],0);
  $bar_data['evening'][] = round($date['evening'],0);
  $block['energy_yesterday'] = render_avg_groupbar_chart($bar_data, 'Yesterday', 200);
  return $block;
}

/*
 * Function to return no data available message
 */
function no_data_available_block($sensorID) {
  if($sensorID != 0 && $sensorID != NULL) {
    $block['no_data_available'] = array(
        '#prefix' => '<div class="no_data">',
        '#markup' => '<h4>' . t('No data is currently available for sensor:') . $sensorID,
        '#suffix' => '</div>',
        );
  } else {
    $block['no_sensor_available'] = array(
        '#prefix' => '<div class="no_data">',
        '#markup' => '<h4>' 
        . t('No sensor is currently available, Please check you configuration.'),
        '#suffix' => '</div>',
        );
  }
  return($block);
}

function last24h_reading_chart_block() {
  if(isset($_REQUEST['sensorID'])) {
    $sensorID=$_REQUEST['sensorID'];
  } else {
    $sensorID=3;
  }
  $temps = calc_average_reading_set(get_last_nhour_temps_db(4,24));
  $powers = calc_average_reading_set(get_last_nhour_powers_db($sensorID,24));
  $block['temp_chart_last24h'] = render_last_nh_reading(4,24,'Temp',$temps);
  $block['power_chart_last24h'] = render_last_nh_reading($sensorID,24,'Power',$powers);
  $temp_range = get_last_nhour_maxmin_temp_db($sensorID,24);
  /*
     $block['temp_range'] = array(
     '#markup' =>  '<h4>Max: ' . $temp_range['max']['temp'] . '</h4>' .
     '<h3> ' . $reading['temp'] . '</h4>' .
     '<h4>Min: ' . $temp_range['min']['temp'] . '</h4>',
     );*/
  return($block);
}

function sensor_list_block($deviceID = NULL, $select = NULL) {
  if($deviceID == NULL) {
    if(isset($_REQUEST['deviceID'])) {
      $deviceID = $_REQUEST['deviceID'];
    } else {
      $deviceID = variable_get('master_device');
    }
  }

  $block = array();
  $sensors = get_devices_sensors($deviceID);
  $header = array(
      'sensorID' => t('sensorID'),
      'name' => t('Name'),
      'description' => t('Description'),
      'type' => t('Measurement Type'),
      'units' => t('Units'),
      );
  $rows=array();
  foreach($sensors[$deviceID]['sensors'] as $sensor) {
    $row=array();
    $row['sensorID'] = '<a href="?q=cc_sensor&sensorID=' 
      . $sensor['ID'] . '">' . $sensor['ID'] . '</a>' ;
    $row['name'] = '<a href="?q=cc_sensor&sensorID=' 
      . $sensor['ID'] . '">' . $sensor['name'] . '</a>' ;
    $row['description'] = '<a href="?q=cc_sensor&sensorID=' 
      . $sensor['ID'] . '">' . $sensor['description'] . '</a>' ;
    $row['type'] = '<a href="?q=cc_sensor&sensorID=' 
      . $sensor['ID'] . '">' . $sensor['type'] . '</a>' ;
    $row['units'] = '<a href="?q=cc_sensor&sensorID=' 
      . $sensor['ID'] . '">' . $sensor['units'] . '</a>' ;
    $rows[$sensor['ID']] = $row;
  }

  $block['pager'] = array('#theme' => 'pager');
  if(!isset($select)) {
    $block['sensors_tableselect'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#empty' => t('No Sensors are available for the selected device.'),
        );
  } else {
    $block['sensors_tableselect'] = array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $rows,
        '#empty' => t('No Sensors are available for the selected device.'),
        '#multiple' => FALSE,
        );
  }
  return $block;
}

function profile_list_block($deviceID = NULL, $select = NULL) {
  if($deviceID == NULL) {
    if(isset($_REQUEST['deviceID'])) {
      $deviceID = $_REQUEST['deviceID'];
    } else {
      $deviceID = variable_get('master_device');
    }
  }

  $block = array();
  $profiles_db = get_devices_profiles_db($deviceID);

  $header = array(
      'name' => t('Profile Name'),
      'description' => t('Profile Description'),
      's_name' => t('Sensor ID:Name'),
      's_description' => t('Sensor Description'),
      'type' => t('Sensor Type'),
      'units' => t('Units'),
      'date' => t('Profile Start Date'),
      'end_date' => t('Profile End Date'),
      );
  $rows=array();
  foreach($profiles_db as $profile) {
    $row=array();
    $row['name'] = '<a href="?q=cc_profile&profileID=' 
      . $profile->profileID . '">' . $profile->p_name . '</a>' ;
    $row['description'] = '<a href="?q=cc_profile&profileID=' 
      . $profile->profileID . '">' . $profile->p_description . '</a>' ;
    $row['s_name'] = '<a href="?q=cc_profile&profileID=' 
      . $profile->profileID . '">' . $profile->sensorID . ':' .$profile->name . '</a>' ;
    $row['s_description'] = '<a href="?q=cc_profile&profileID=' 
      . $profile->profileID . '">' . $profile->description . '</a>' ;
    $row['type'] = '<a href="?q=cc_profile&profileID=' 
      . $profile->profileID . '">' . $profile->type . '</a>' ;
    $row['units'] = '<a href="?q=cc_profile&profileID=' 
      . $profile->profileID . '">' . $profile->units . '</a>' ;
    $row['date'] = '<a href="?q=cc_profile&profileID=' 
      . $profile->profileID . '">' . $profile->date . '</a>' ;
    $row['end_date'] = '<a href="?q=cc_profile&profileID=' 
      . $profile->profileID . '">' . $profile->end_date . '</a>' ;
    $rows[$profile->profileID] = $row;
  }

  $block['pager'] = array('#theme' => 'pager');
  if(!isset($select)) {
    $block['sensors_tableselect'] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#empty' => t('No Sensors are available for the selected device.'),
        );
  } else {
    $block['sensors_tableselect'] = array(
        '#type' => 'tableselect',
        '#header' => $header,
        '#options' => $rows,
        '#empty' => t('No Sensors are available for the selected device.'),
        '#multiple' => FALSE,
        );
  }
  return $block;
}
