<?php


/**
 * Page to display basic consumption information similar to the
 * current cost devices
 */
function currentcost_client_form() {
  /* css */
  drupal_add_css(drupal_get_path('module', 'currentcost_client') . '/currentcost_client.css');

  return drupal_get_form('currentcost_client_my_form');
}


/**
 * This function is called the "form builder". It builds the form.
 * Notice, it takes one argument, the $form_state
 */
function currentcost_client_my_form($form_state) {

  $form = array();

  $deviceID = get_deviceID();

  $device = get_device_master_sensors($deviceID);
  /* Start Page */
  $power_cur = get_current_power_reading_db($device['power']['ID']);

  $form['page_container'] = array(
      '#markup' => '<div id="cc_container">'
      . '<h2>' . t($device['name'] . ' - ' . $device['description']) . '</h2>',
      );
  $form['row_container_1'] = array(
      '#markup' => '<div id="cc_row_1">'
      );
  $form['current_power'] = element_wrapper(
      render_current_power($power_cur['measurement']),
      'Current Power Consumption',
      'current_power',
      1);

  $form['power_stats'] = element_wrapper(
      current_power_stats_block($device['power']['ID']),
      'Current Power Stats',
      'current_power_stats',
      2);

  $form['row_container_1_end'] = array(
      '#markup' => '</div>'
      );
  $form['row_container_2'] = array(
      '#markup' => '<div id="cc_row_2">'
      );

  $form['yesterday_avg'] = element_wrapper(
      yesterday_avg_block($device['power']['ID']),
      'Yesterday - Daily Averages',
      'yest_daily_avgs',
      1);

  /* Last 7 days avgs bar chart */
  $dates = get_last_x_dates(7);
  $form['last7days_avg'] = element_wrapper(
      render_avg_bar_chart(
        get_date_calc($device['power']['ID'], $dates, 'DAY', 'AVG')
        , 'Last 7 days', 210),
      'Last 7 days - Averages',
      'last7days_avg',
      2);

  $form['row_container_2_end'] = array(
      '#markup' => '</div>'
      );
  $form['row_container_3'] = array(
      '#markup' => '<div id="cc_row_3">'
      );

  $form['last_reading_date'] = element_wrapper(
      render_last_reading_date($power_cur['date']),
      'Last Reading Date',
      'last_reading_date',
      1);
  $temp_cur = get_current_temp_reading_db($device['temp']['ID']);
  $form['current_temp'] = element_wrapper(
      render_current_temp($temp_cur['measurement']),
      'Current Temperature',
      'current_temp',
      2);

    $form['row_container_3_end'] = array(
        '#markup' => '<div class="hrule"><hr></div></div>'
        );

  $form['device_fieldset'] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div id="cc_device_fieldset">',
      '#suffix' => '</div>',
      '#title' => t('Select a device or sensor below to change the current display'),
      );
  $devices = get_device_list();
  $form['device_fieldset']['device'] = array(
      '#type' => 'select',
      '#title' => t('Device:'),
      '#options' =>  $devices,
      '#default_value' => $deviceID,
      '#attributes' => array('onchange' => "form.submit('results')"),
      );

  $form['device_fieldset']['sensors'] = sensor_list_block($deviceID);
  $form['page_container_end'] = array(
      '#markup' => '</div>'
      );

  /* hidden submit button - required for onchange attribute*/
  $form['submit'] = array(
      '#type' => 'submit',
      '#title' => t('Submit'),
      '#value' => 'Submit',
      '#attributes' => array('style' => 'display:none;'),
      );
  return $form;
}

/* 
 * currentcost_client_my_form submit handler
 */
function currentcost_client_my_form_submit($form, &$form_state) {
  $form_state['redirect'] = array(
      'cc_client',
      array(
        'query' => array(
          'deviceID' => $form_state['values']['device'],),
        ),	
      );
}
