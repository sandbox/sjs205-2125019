<?php

/**
 * Page to display last 24 hours information 
 */
function currentcost_client_sensor_form() {
  /* css */
  drupal_add_css(drupal_get_path('module', 'currentcost_client') . '/currentcost_client.css');

  return drupal_get_form('currentcost_client_sensor_my_form');
}

/**
 * This function is called the "form builder". It builds the form.
 * Notice, it takes one argument, the $form_state
 */
function currentcost_client_sensor_my_form($form_state) {

  $form = array();

  $dates = get_last_x_dates(7);

  $sensor = array();
  $sensor['ID'] = get_sensorID();
  $sensor =  get_sensor($sensor['ID']);
  if(isset($_REQUEST['deviceID'])) {
    $deviceID = $_REQUEST['deviceID'];
  } else {
    $deviceID = $sensor['deviceID'];
  }
  $devices = get_device_list();

  /* Start Page */

  $sensor['current_reading'] = get_reading_db($sensor['ID']);
  $form['page_container'] = array(
      '#markup' => '<div id="cc_container">'
      );
  $form['fieldset_div'] = array(
      '#markup' => '<div id="cc_fieldset">'
      . '<h2>' . t($sensor['name'] . ' - ' . $sensor['description']) . '</h2>',
      );

  $form['row_container_1'] = array(
      '#markup' => '<div id="cc_row_1">'
      );
  if($sensor['typeID'] == 1) {
    $form['current_power'] = element_wrapper(
        render_current_power($sensor['current_reading']['measurement']),
        'Current Power Reading',
        'current_power',
        1);

    $form['power_stats'] = element_wrapper(
        current_power_stats_block($sensor['ID']),
        'Power Stats',
        'power_stats',
        2);
  } elseif($sensor['typeID'] == 9) {
    $form['current_temp'] = element_wrapper(
        render_current_temp($sensor['current_reading']['measurem    ent']),
        'Current Temperature Reading',
        'current_temp',
        1);

    $form['last_reading_date'] = element_wrapper(
        render_last_reading_date($power_cur['date']),
        'Last Reading Date',
        'last_reading_date',
        2);
  }
  $form['row_container_1_end'] = array(
      '#markup' => '</div>'
      );

  $form['row_container_2'] = array(
      '#markup' => '<div id="cc_row_2">'
      );
  $sensor['measurements'] = 
    calc_average_reading_set(get_last_nhour_measurements_db($sensor['ID'], 24), 24*60);
  $form['last_24h_chart'] = element_wrapper(
    render_last_nh_reading(24,$sensor['name'],$sensor['measurements'], 650),
      'Last 24 hours - Averages',
      'last_24h_avg',
      'x');

  $form['row_container_2_end'] = array(
      '#markup' => '</div>'
      );
  $form['row_container_3'] = array(
      '#markup' => '<div id="cc_row_3">'
      );
  $grouped_bar_data = get_dates_interval_avgs($sensor['ID'], $dates);
  $form['daily_avgs'] = element_wrapper(
      render_avg_groupbar_chart($grouped_bar_data,$sensor['measurements'], 650),
        'Last 7 Days - Daily Averages',
        'daily_avgs',
        'x');

      $form['row_container_3_end'] = array(
        '#markup' => '</div>'
        );
      $form['row_container_4'] = array(
        '#markup' => '<div id="cc_row_4">'
        );
      
      $dates = get_last_x_dates(31);
  $form['last_month_daily_avg'] = element_wrapper(
    render_avg_bar_chart(get_date_calc($sensor['ID'], $dates, 'DAY', 'AVG'), 'Last Month', 650),
      'Last Months Averages',
      'last_month_daily_avg',
      'x');

      $form['row_container_4_end'] = array(
        '#markup' => '<div class="hrule"><hr></div></div>'
        );
      /* Device/Sensor selection. */
      $form['device'] = array(
          '#prefix' => '<p>'.t('Select a device below to change the current display.').'</p>',
          '#type' => 'select',
          '#title' => t('Device:'),
          '#options' =>  $devices,
          '#default_value' => $deviceID,
          '#attributes' => array('onchange' => "form.submit('results')"),
          );
      $form['sensors'] = sensor_list_block($deviceID);

      $form['page_container_end'] = array(
          '#markup' => '</div>'
          );

      return $form;
}
/* 
 * currentcost_client_sensor_my_form submit handler
 */
function currentcost_client_sensor_my_form_submit($form, &$form_state) {
  $form_state['redirect'] = array(
      'cc_sensor',
      array(
        'query' => array(
          'deviceID' => $form_state['values']['device'],),
        ),	
      );
}
