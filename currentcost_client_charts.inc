<?php

/*
 * The following functions render charts showing device information as $form elements
 */

function render_current_power($power) {
  /* Format max chart value */
  if($power <= 100) {
    $adjust_max = round($power+10, -1);
  } elseif($power <= 1000) {
    $adjust_max = round($power+100, -2);
  } elseif($power <= 10000) {
    $adjust_max = round($power+1000, -3);
  }

  if($adjust_max > variable_get('currentcost_client_power_goal', 250)) {
    $end_colour = 'ff0000';
  } else { 
    $end_colour = 'ffcc00';
  }

  /* Create chart instance */
  $form['power_consumption_gmeter'] = array(
      '#theme' => 'chart',
      '#chart_id' => 'current_power_consumption',
      '#title' => '',
      '#type' => CHART_TYPE_GMETER,
      '#size' => chart_size(250, 150),
      '#adjust_resolution' =>array(
        '#adjust' => TRUE,
        '#max' => $adjust_max,
        ),
      '#adjust_resolution' => array('#adjust' => TRUE, '#max' => $adjust_max),
      '#bar_size' => chart_bar_size($adjust_max),
      '#data_colors' => array('10ff00',  $end_colour),
      '#data' => array(
        'Power' => $power,
        ),  
      );  
  $form['power_consumption_gmeter']['#mixed_axis_labels'][CHART_AXIS_Y_LEFT][0][] = chart_mixed_axis_range_label(0, $adjust_max);
  /* Format and write labels */
  $form['power_consumption_gmeter']['#mixed_axis_labels'][CHART_AXIS_X_BOTTOM][2][] = chart_mixed_axis_label(t($power . ' Watts'), 50);
  return($form);

}

function render_current_temp($temp) {

  /* Format max chart value */
  if($temp <= 100) {
    $adjust_max = round($temp+10, -1);
  } elseif($temp <= 1000) {
    $adjust_max = round($temp+100, -2);
  }
  if($adjust_max > variable_get('currentcost_client_power_goal', 250)) {
    $end_colour = 'ff0000';
  } else { 
    $end_colour = 'ffcc00';
  }

  /* Create chart instance */
  $form['temp_gmeter'] = array(
      '#theme' => 'chart',
      '#chart_id' => 'temperature',
      '#title' => '',
      '#type' => CHART_TYPE_GMETER,
      '#size' => chart_size(250,150),
      '#data_colors' => array('0000ff', $end_colour),
      '#adjust_resolution' =>array(
        '#adjust' => TRUE,
        '#max' => $adjust_max,
        ),
      '#data' => array(
        'Temp' => ($temp),
        ),  
      );  
  $form['temp_gmeter']['#mixed_axis_labels'][CHART_AXIS_Y_LEFT][0][] = chart_mixed_axis_range_label(0, $adjust_max);
  /* Format and write labels */
  $form['temp_gmeter']['#mixed_axis_labels'][CHART_AXIS_X_BOTTOM][2][] = chart_mixed_axis_label(t($temp . ' ' . "&#176;" . 'C'), $adjust_max);
  return($form);

}


function render_last_nh_reading($minutes,$type,$readings,$width = 800) {
  if($minutes >=60) {
    $period = round($minutes/60,2);
    $units = 'hours';
  } else {
    $period = $minutes;
    $units = 'minutes';
  }

  $reading_max = max($readings['measurement']);
  if($reading_max <= 100) {
    $adjust_max = round($reading_max+10, -1);
  } elseif($reading_max <= 1000) {
    $adjust_max = round($reading_max+100, -2);
  } elseif($reading_max <= 10000) {
    $adjust_max = round($reading_max+1000, -3);
  } elseif($reading_max <= 100000) {
    $adjust_max = round($reading_max+10000, -4);
  }
  $form['line_chart_last_' . $period . '_' . $type] = array(
      '#theme' => 'chart',
      '#chart_id' => 'line_chart_last_' . $period . '_' . $type,
      '#title' => '',
      '#type' => CHART_TYPE_LINE,
      '#size' => chart_size($width, 200),
      '#adjust_resolution' =>array(
        '#adjust' => TRUE,
        '#max' => $adjust_max,
        ),
      '#data_colors' => array('0000ff', 'ff0000'),
      ); 
  $form['line_chart_last_' . $period . '_' . $type]['#data'][] = array($readings['measurement']);
  /* Format and write labels */
  $form['line_chart_last_' . $period . '_' . $type]['#mixed_axis_labels'][CHART_AXIS_Y_LEFT][0][] = chart_mixed_axis_range_label(0, $adjust_max);
  $form['line_chart_last_' . $period . '_' . $type]['#mixed_axis_labels'][CHART_AXIS_X_BOTTOM][2][] = chart_mixed_axis_range_label(-$period, 0);
  return($form);
}

function render_avg_bar_chart($data, $type, $width = 800) {
  $reading_max = max($data);
  $reading_count = count($data);

  if($reading_max <= 100) {
    $adjust_max = round($reading_max+10, -1);
  } elseif($reading_max <= 1000) {
    $adjust_max = round($reading_max+100, -2);
  } elseif($reading_max <= 10000) {
    $adjust_max = round($reading_max+1000, -3);
  } elseif($reading_max <= 100000) {
    $adjust_max = round($reading_max+10000, -4);
  }

  $data_count = count($data);
  $bar_width = round(($width - 20 - (($data_count * 7)))/$data_count, 0);

  $form['avg_bar_chart_'. $type] = array(
      '#theme' => 'chart',
      '#chart_id' => 'avg_bar_chart_' . $type,
      '#title' => '',
      '#type' => CHART_TYPE_BAR_V,
      '#size' => chart_size($width, 200),
      '#bar_size' => chart_bar_size($bar_width,5),
      '#adjust_resolution' =>array(
        '#adjust' => TRUE,
        '#max' => $adjust_max,
        ),
      '#data_colors' => array('FF8000'),
      ); 
  $form['avg_bar_chart_'. $type]['#data'][] = array($data);
  /* Format and write labels */
  $form['avg_bar_chart_' . $type]['#mixed_axis_labels'][CHART_AXIS_Y_LEFT][0][] = chart_mixed_axis_range_label(0, $adjust_max);
  $form['avg_bar_chart_' . $type]['#mixed_axis_labels'][CHART_AXIS_X_BOTTOM][2][] = chart_mixed_axis_range_label(-count($data),-1);
  return($form);

}

function render_avg_groupbar_chart($group_data, $type, $width = 1000) {

  $data_count = count($group_data['night']) * 4;
  $bar_width = round(($width - (100 +($data_count * 7)))/$data_count, 0);

  $form['avg_bar_chart_grouped'. $type] = array(
      '#theme' => 'chart',
      '#chart_id' => 'avg_bar_chart_grouped' . $type,
      '#title' => '',
      '#type' => CHART_TYPE_BAR_V_GROUPED,
      '#size' => chart_size($width, 200),
      '#bar_size' => chart_bar_size($bar_width, 5),
      '#grid_lines' => chart_grid_lines((4*( $bar_width+5)),5),
      );
  $form['avg_bar_chart_grouped'. $type]['#data_colors'][] = chart_unique_color('night');
  $form['avg_bar_chart_grouped'. $type]['#data_colors'][] = chart_unique_color('morning');
  $form['avg_bar_chart_grouped'. $type]['#data_colors'][] = chart_unique_color('afternoon');
  $form['avg_bar_chart_grouped'. $type]['#data_colors'][] = chart_unique_color('evening');
  foreach($group_data as $data) {
    $form['avg_bar_chart_grouped'. $type]['#data'][]= $data;
    $max_val[] = max($data);
  }

  /* adjust max value */
  $reading_max = max($max_val);

  if($reading_max <= 100) {
    $adjust_max = round($reading_max+10, -1);
  } elseif($reading_max <= 1000) {
    $adjust_max = round($reading_max+100, -2);
  } elseif($reading_max <= 10000) {
    $adjust_max = round($reading_max+1000, -3);
  } elseif($reading_max <= 100000) {
    $adjust_max = round($reading_max+10000, -4);
  }
  $form['avg_bar_chart_grouped' . $type]['#adjust_resolution'] = array(
      '#adjust' => TRUE,
      '#max' => $adjust_max,
      );

  //$form['avg_bar_chart_grouped' . $type]['#grid_lines'] => chart_grid_lines(30, 15)m
  $form['avg_bar_chart_grouped'. $type]['#legends'][]= t('Night');
  $form['avg_bar_chart_grouped'. $type]['#legends'][]= t('Morning');
  $form['avg_bar_chart_grouped'. $type]['#legends'][]= t('Afternoon');
  $form['avg_bar_chart_grouped'. $type]['#legends'][]= t('Evening');
  /* Format ['evening']and write labels */
  $form['avg_bar_chart_grouped'. $type]['#mixed_axis_labels'][CHART_AXIS_Y_LEFT][0][] = chart_mixed_axis_range_label(0, $adjust_max);
  return($form);

}

function render_last_reading_date($date) {
  $reading_date = strtotime($date); 
  $now = strtotime('now');
  $block['last_reading_date'] =array(
      '#markup' => '<div class="reading_date">' 
      . '<h4>' . date('d/m/Y', $reading_date) . '</h4>'
      . '<h2' . ($reading_date < $now-900 ? ' style="color:red;">': '>')
      . date('H:i:s', $reading_date) . '</h2>'
      . '</div>'
      );  
  return($block);
}
