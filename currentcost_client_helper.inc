<?php
/*
 * The following functions are helper functions for the currentcost client
 */


function element_wrapper($form_data, $title, $class, $col) {
  $block['fieldset_' . $class] = array(
      '#type' => 'fieldset',
      '#prefix' => '<div class="cc_col' . $col . '">',
      '#suffix' => '</div>',
      '#title' => t($title),
      );
  $block['fieldset_' . $class]['element'] = $form_data;
  $block['fieldset_' . $class]['element']['#prefix'] =
      '<div  id="' . $class . '">';
  $block['fieldset_' . $class]['element']['#suffix'] = '</div>';

  return($block);
}


/*
 * The following function returns the deviceID
 */
function get_deviceID($sensorID = NULL) {

  /* Get deviceID */
  if(isset($_REQUEST['deviceID'])) {
    $deviceID = $_REQUEST['deviceID'];
  } else {

    $deviceID = variable_get('master_device');
  } 
  return($deviceID);
}

/*
 * Function to return the maximum and minimum readings from a dataset
 */
function get_readings_maxmin($profile_db) {
  foreach($profile_db as $date => $read) {
    $tmp_readings[] = $read->measurement;
    $tmp_date[] = $date;
  }
  $min = min($tmp_readings);
  $min_date = $tmp_date[array_search($min, $tmp_readings)];
  $max = max($tmp_readings);
  $max_date = $tmp_date[array_search($max, $tmp_readings)];
  return array(
      'min' => array(
        'reading' => $min,
        'date' => $min_date),
      'max' => array(
        'reading' => $max,
        'date' => $max_date),
      );
}

/*
 * The following function returns the sensorID
 */
function get_sensorID() {

  /* Get sensorID */
  if(isset($_REQUEST['sensorID'])) {
    $sensorID = $_REQUEST['sensorID'];
  } else {
    $sensorID = variable_get('master_sensor_power', 1);
  }
  return($sensorID);
}

/* 
 * function to return a list of devices
 */
function get_device_list() {
  $devices_db = get_devices_db();
  foreach($devices_db as $id=>$device) {
    $devices[$id] = $device->deviceID . ':' . $device->name;
  }
  return($devices);
}

function get_device_master_sensors($deviceID) {
  /* Need to make this function more generic. *

  /* Get sensors from database */
  $devices = get_devices_sensors($deviceID);
  $devices[$deviceID]['temp']['exists'] = $devices[$deviceID]['temp']['master'] = FALSE;
  $devices[$deviceID]['power']['exists'] = $devices[$deviceID]['power']['master'] = FALSE;
  foreach($devices[$deviceID]['sensors'] as $sensor) {
    if($sensor['typeID'] == 1){
      $devices[$deviceID]['power']['exists'] = TRUE;
      if($sensor['master'] == 1) {
        /* Master electricity sensor */
        $devices[$deviceID]['power']['master'] = TRUE;
        $devices[$deviceID]['power']['ID'] = $sensor['ID'];
      }
    } else if($sensor['typeID'] == 9) {
      $devices[$deviceID]['temp']['exists'] = TRUE;
      if($sensor['master'] == 1) {
        /* Master Temp sensor */
        $devices[$deviceID]['temp']['master'] = TRUE;
        $devices[$deviceID]['temp']['ID'] = $sensor['ID'];
      }
    }
  }
  if($devices[$deviceID]['power']['master'] == FALSE) {
    /* no master has been set */
    if($devices[$deviceID]['power']['exists'] == TRUE) {
      /* Power sensors are available */
      foreach($devices[$deviceID]['sensors'] as $sensor) {
        if($sensor['typeID'] == 1){
          /* Assign first available sensor */
          /* default Master electricity sensor */
          $devices[$deviceID]['power']['ID'] = $sensor['ID'];
          break;
        }
      }
    } else {
      /* No Power devices available */
      $devices[$deviceID]['power']['ID'] = 0;
    }
  }
  if($devices[$deviceID]['temp']['master'] == FALSE) {
    /* no master has been set */
    if($devices[$deviceID]['temp']['exists'] == TRUE) {
      /* Temp sensors are available */
      foreach($devices[$deviceID]['sensors'] as $sensor) {
        if($sensor['typeID'] == 9){
          /* Assign first available sensor */
          /* default Master Temp sensor */
          $devices[$deviceID]['temp']['ID'] = $sensor['ID'];
          break;
        }
      }
    } else {
      /* No Power devices available */
      $devices[$deviceID]['temp']['ID'] = 0;
    }
  }
  return($devices[$deviceID]);
}


function calc_kwh($power) {
  /* assuming kWh at $power over 1 hour */
  return($power/1000);
}

/* 
 * Function to return the average reading set over a given interval, in hours
 */
function calc_average_reading_set($readings_db, $minutes,$start_time = NULL) {
  
  $i = 0;
  $count = 0;
  $temp_date;
  $tmp_powers=array();
  $count_ = 0;
  $tmp_count = 0;
  $readings = array();
  /* interval calculation => 200 samples */
  $readings['sample_interval_secs'] = ($minutes * 60) / 200;
  foreach($readings_db as $date => $read) {
    if($i == 0) {
      if($start_time == NULL) {
        $start = strtotime($date);
      } else {
        $start = strtotime($start_time);
      }
      if(strtotime($date) > ($start + $readings['sample_interval_secs'])) {
        /* Accounts for missing points at the start of a set */
        $diff = round((strtotime($date) - $start)/$readings['sample_interval_secs'],0);
        for($c = 0; $c < $diff; $c++) {
          $count++;
          $readings['measurement'][] = 0;
          $readings['date'][] = 
            date('Y-m-d H:i:s', $start+($c*$readings['sample_interval_secs']));
        }
      }
      $readings['measurement'][] = $read->measurement;
      $readings['date'][] = $date;
      $count++; 
      $tmp_count = 1;
      $tmp_date = strtotime($date);
    } else {
      if(strtotime($date) > ($tmp_date + $readings['sample_interval_secs'])) { 
        if(strtotime($date) > ($tmp_date + 2*$readings['sample_interval_secs'])) { 
          /* Accounts for missing points in the middle of a data set */
          /* Approximates data by taking the difference between the current 
             and previous measurement... could probably be imroved. */
          $diff = round((strtotime($date) - $tmp_date)/$readings['sample_interval_secs'],0);
          $prev_measurement = $readings['measurement'][$count-1];
          $value = ($read->measurement - $prev_measurement) / $diff;
          for($c = 1; $c <= $diff; $c++) {
            $count++;
            $readings['measurement'][] = $prev_measurement + ($value * $c);
            $readings['date'][] = 
              date('Y-m-d H:i:s', $start+(($c-1)*$readings['sample_interval_secs']));
          }
        }
        $tmp_readings[]=$read->measurement;
        $tmp_count++;
        $count++; 
        $tmp_date = strtotime($date);
        $readings['measurement'][] = (array_sum($tmp_readings)/$tmp_count);
        $readings['date'][] = $date;
        unset($tmp_readings);
        $tmp_count = 0;
      } else {
        $tmp_readings[]=$read->measurement;
        $tmp_count++;
      }
    }
    $i++;
  }
  $readings['count'] = $count; 
  return($readings);
}


/*
 * Functions to calc and return the kwh for a power reading
 */
function calc_watts_kwh($power) {
  return($power/1000);
}
/*
 * Functions to calc and return the cost per day for a kwh reading
 */
function calc_kwh_day_cost($kwh) {
  return(round($kwh*24*variable_get('currentcost_client_unit_rate', 0.1457),2));
}
/*
 * Functions to return the last 7 dates inc today
 */
function get_last_x_dates($x) {
  $dates = array();
  $now = strtotime('now');
  for($i=($x-1);$i>=0;$i--) {
    $dates[] = date('Y-m-d',$now-(($i) * 86400));
  }
  return($dates);
}
/*
 * Function returns the average measurements for each of the given dates' intervals
 * - The function check t ensure that the given date is not in the database 
 * - Once the calc has been carried out, it is inserted into the database
 */
function get_dates_interval_avgs($sensorID, $dates) {
  foreach($dates as $date) {
    $dates[$date]['calc']['period'] = get_day_interval_avgs($sensorID, $date);
    $data['night'][] = round($dates[$date]['calc']['period']['night'],0);
    $data['morning'][] = round($dates[$date]['calc']['period']['morning'],0);
    $data['afternoon'][] = round($dates[$date]['calc']['period']['afternoon'],0);
    $data['evening'][] = round($dates[$date]['calc']['period']['evening'],0);
  }
  return($data);
}

/*
 * Function returns the average measurements for each of the given dates
 * - The function check t ensure that the given date is not in the database 
 * - Once the calc has been carried out, it is inserted into the database
 */
function get_date_calc($sensorID, $dates, $calc_type) {
  $results = array();
  foreach($dates as $date) {
    $dates[$date]['calc'] = get_sensor_calc_db($sensorID,$calc_type,$date,'DAY');
    if($dates[$date]['calc'] == FALSE) {

      $result= get_date_avg_db($sensorID,$date);
      $dates[$date]['calc']['result'] = round($result['expression'],2);
      if(date('Y-m-d') == date('Y-m-d', strtotime($date))) {
        /* today */
        insert_sensor_calc_db($sensorID,$calc_type,$date,'DAY',$dates[$date]['calc']['result'],
            date('Y-m-d H:i:s',strtotime('now')+900));
      } else {
        insert_sensor_calc_db($sensorID,$calc_type,$date,'DAY',$dates[$date]['calc']['result']);
      }
    }
    $results[]=$dates[$date]['calc']['result'];
  }
  return($results);
}

/*
 * function to return all interval avgs for a certian date
 */
function get_day_interval_avgs($sensorID, $date) {
  return(array(
        'morning' => get_morning_avg($sensorID, $date),
        'afternoon' => get_afternoon_avg($sensorID, $date),
        'evening' => get_evening_avg($sensorID, $date),
        'night' => get_night_avg($sensorID, $date),
        ));
}
/*
 * function to return all night interval avgs for a certian date
 */
function get_night_avg($sensorID, $date) {
  $tmp = get_sensor_calc_db($sensorID,'AVG',$date,'NIGHT');
  if($tmp == FALSE) {
    $tmp = 
      get_interval_avg_db($sensorID, $date . ' 00:00:00', $date . ' 05:59:59');
    $result = round($tmp['expression'],2);
    if(date('Y-m-d H:i:s') <= $date . ' 05:59:59') {
      insert_sensor_calc_db($sensorID,'AVG',$date,'NIGHT',$result,
          date('Y-m-d H:i:s',strtotime('now')+900));
    } else {
      insert_sensor_calc_db($sensorID,'AVG',$date,'NIGHT',$result);
    }
  } else {
    $result = $tmp['result'];
  }

  return($result);
}
/*
 * function to return all morning interval avgs for a certian date
 */
function get_morning_avg($sensorID, $date) {
  $tmp = get_sensor_calc_db($sensorID,'AVG',$date,'MORN');
  if($tmp == FALSE) {
    $tmp = 
      get_interval_avg_db($sensorID, $date . ' 06:00:00', $date . ' 11:59:59');
    $result = round($tmp['expression'],2);
    if(date('Y-m-d H:i:s') <= $date . ' 11:59:59') {
      insert_sensor_calc_db($sensorID,'AVG',$date,'MORN',$result,
          date('Y-m-d H:i:s',strtotime('now')+900));
    } else {
      insert_sensor_calc_db($sensorID,'AVG',$date,'MORN',$result);
    }
  } else {
    $result = $tmp['result'];
  }
  return($result);
}
/*
 * function to return all afternoon interval avgs for a certian date
 */
function get_afternoon_avg($sensorID, $date) {
  $tmp = get_sensor_calc_db($sensorID,'AVG',$date,'NOON');
  if($tmp == FALSE) {
    $tmp = 
      get_interval_avg_db($sensorID, $date . ' 12:00:00', $date . ' 17:59:59');
    $result = round($tmp['expression'],2);
    if(date('Y-m-d H:i:s') <= $date . ' 17:59:59') {
      insert_sensor_calc_db($sensorID,'AVG',$date,'NOON',$result,
          date('Y-m-d H:i:s',strtotime('now')+900));
    } else {
      insert_sensor_calc_db($sensorID,'AVG',$date,'NOON',$result);
    }
  } else {
    $result = $tmp['result'];
  }
  return($result);
}
/*
 * function to return all evening interval avgs for a certian date
 */
function get_evening_avg($sensorID, $date) {
  $tmp = get_sensor_calc_db($sensorID,'AVG',$date,'EVE');
  if($tmp == FALSE) {
    $tmp = 
      get_interval_avg_db($sensorID, $date . ' 18:00:00', $date . ' 23:59:59');
    $result = round($tmp['expression'],2);
    if(date('Y-m-d H:i:s') <= $date . ' 23:59:59') {
      insert_sensor_calc_db($sensorID,'AVG',$date,'EVE',$result,
          date('Y-m-d H:i:s',strtotime('now')+900));
    } else {
      insert_sensor_calc_db($sensorID,'AVG',$date,'EVE',$result);
    }
  } else {
    $result = $tmp['result'];
  }
  return($result);
}

/*
 * Function to return the interval between two dates, in minutes
 *  $date_n should be in mysql form. 
 */
function get_dates_interval_minutes($date_1, $date_2 = NULL) {
  $date_str_1 = strtotime($date_1);
  if($date_2 == NULL) {
    $date_str_2 = time();
  } else {
    $date_str_2 = strtotime($date_2);
  }

  return(abs(round(($date_str_1 - $date_str_2) / 60,0)));
}

/*
 * The following functions are wrappers for the database functions and 
 *  return db queries in a know format
 */

/* 
 * Function to return a device array from the database
 */
function get_devices_sensors($deviceID = NULL) {

  $device_db = get_devices_sensors_db($deviceID);
  $devices=array();
  foreach($device_db as $sensor) {
    $deviceID = $sensor->deviceID;
    if(!isset($devices[$deviceID])) {
      $devices[$deviceID] = array(
          'ID' => $deviceID,
          'name' => $sensor->name,
          'description' => $sensor->description,
          'sensors' => array(),
          );
    }

    $devices[$deviceID]['sensors'][$sensor->sensorID] = array(
        'ID' => $sensor->sensorID,
        'name' => $sensor->s_name,
        'description' => $sensor->s_description,
        'units' => $sensor->units,
        'type' => $sensor->type,
        'type_desc' => $sensor->t_description,
        'master' => $sensor->master,
        'typeID' => $sensor->typeID,
        'deviceID' => $sensor->deviceID,
        );
  }
  return($devices);
}


/* 
 * Function to return a device's profiles array from the database
 */
function get_device_profiles($deviceID = NULL) {
/* need to write this function, at the moment this will only be 
   called buy the profile_list_block function */
}

/* 
 * Function to return a sensor array from the database
 */
function get_sensor($sensorID) {
  $sensor_db = get_sensor_db($sensorID);
  return(array(
        'ID' => $sensor_db['sensorID'],
        'name' => $sensor_db['name'],
        'description' => $sensor_db['s_description'],
        'units' => $sensor_db['units'],
        'type' => $sensor_db['type'],
        'type_desc' => $sensor_db['description'],
        'master' => $sensor_db['master'],
        'typeID' => $sensor_db['typeID'],
        'deviceID' => $sensor_db['deviceID'],
        ));
}

/* 
 * Function to return a profile array from the database
 */
function get_profile($profileID) {

  $profile_db = get_profile_db($profileID);
  return(array(
        'ID' => $profileID,
        'name' => $profile_db['name'],
        'description' => $profile_db['description'],
        'date' => $profile_db['date'],
        'end_date' => $profile_db['end_date'],
        'sensor' => array(
          'ID' => $profile_db['sensorID'],
          'name' => $profile_db['s_name'],
          'description' => $profile_db['s_description'],
          'units' => $profile_db['units'],
          'type' => $profile_db['type'],
          'type_desc' => $profile_db['t_description'],
          'master' => $profile_db['master'],
          'typeID' => $profile_db['typeID'],
          'deviceID' => $profile_db['deviceID'],
          ),
        ));
}

/* 
 * Function to return a profile's readings from the database
 */
function get_profile_readings($profileID) {

  $readings_db = get_profile_readings_db($profileID);

  foreach($readings_db AS $reading) {
    $readings[] = array(
        'reading' => $reading->measurement,
        'date' => $reading->date
        );
  }
  return($readings);
}
